@extends('Frontend.Member.Layouts.app')
@section('content')
<div class="col-sm-12">
    <div class="signup-form"><!--sign up form-->
        <h2>New User Signup!</h2>
        <form method="POST" action="{{ route('memberreg') }}" enctype="multipart/form-data">
            @csrf
            <input type="text" placeholder="Name" name="name"/>
            <input type="email" placeholder="Email Address" name="email"/>
            <input type="password" placeholder="Password" name="pass"/>
            <input type="text" placeholder="Phone" name="phone" />
            <input type="file" name="filesimg" >
            <input type="text" placeholder="Address" name="address" />

            <button type="submit" class="btn btn-default">Signup</button>
        </form>
    </div><!--/sign up form-->
</div>
@endsection

