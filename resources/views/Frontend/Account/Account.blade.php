@extends('Frontend.Layouts.app2')
@section('content3')

		<div class="col-sm-8">
			<div class="mainmenu pull-left">
					<ul class="nav navbar-nav collapse navbar-collapse">
							<li><a href="{{ url('member/account') }}">Account</a></li>
                          
                            <li><a href="{{ url('member/product') }}">My Products</a></li>
					</ul>
    		</div>	
		</div>

        <div class="col-sm-4">
        			 @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Thông báo</h4>
                                {{session('success')}}
                                </div>
                                @endif

                                @if($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i> Thông báo!<in4>
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                    <li> {{$error}}</i>
                                    @endforeach
                                    </ul>
                                </div>
                                @endif	

					<div class="signup-form" style="">
						<h2>User Update !</h2>
						<form action="{{ route('account_update') }}" method="POST" enctype="multipart/form-data">
							@csrf
							<input type="text" name="name" value="{{ Auth::user()->name }}" >

							<input type="email" name="email" value="{{ Auth::user()->email }}">

							<input type="password" name="password" value="">

                            <input type="text" name="address" value="{{ Auth::user()->address }}">

                            <input type="text" name="id_country" value="{{ Auth::user()->id_country }}">

                            <input type="text" name="phone" value="{{ Auth::user()->phone }}">

                            <input type="file" name="avatar" value="">

							<button type="submit" class="btn btn-default">OK</button>
						</form>
					</div>
				</div>

    <!--/form-->


@endsection
