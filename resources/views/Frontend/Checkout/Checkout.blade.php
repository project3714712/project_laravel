@extends('Frontend.Layouts.app2')
@section('content3')

	<section id="cart_items">
		<div class="container">
			@unless(Auth::check())
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>
			<div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div><!--/checkout-options-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3">
						<div class="shopper-info">
							<p>Shopper Information</p>
							<form>
								<input type="text" placeholder="Display Name">
								<input type="text" placeholder="User Name">
								<input type="password" placeholder="Password">
								<input type="password" placeholder="Confirm password">
							</form>
							<a class="btn btn-primary" href="">Get Quotes</a>
							<a class="btn btn-primary" href="">Continue</a>
						</div>
					</div>
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<p>Bill To</p>
							<div class="form-one">
								<form>
									<input type="text" placeholder="Company Name">
									<input type="text" placeholder="Email*">
									<input type="text" placeholder="Title">
									<input type="text" placeholder="First Name *">
									<input type="text" placeholder="Middle Name">
									<input type="text" placeholder="Last Name *">
									<input type="text" placeholder="Address 1 *">
									<input type="text" placeholder="Address 2">
								</form>
							</div>
							<div class="form-two">
								<form>
									<input type="text" placeholder="Zip / Postal Code *">
									<select>
										<option>-- Country --</option>
										<option>United States</option>
										<option>Bangladesh</option>
										<option>UK</option>
										<option>India</option>
										<option>Pakistan</option>
										<option>Ucrane</option>
										<option>Canada</option>
										<option>Dubai</option>
									</select>
									<select>
										<option>-- State / Province / Region --</option>
										<option>United States</option>
										<option>Bangladesh</option>
										<option>UK</option>
										<option>India</option>
										<option>Pakistan</option>
										<option>Ucrane</option>
										<option>Canada</option>
										<option>Dubai</option>
									</select>
									<input type="password" placeholder="Confirm password">
									<input type="text" placeholder="Phone *">
									<input type="text" placeholder="Mobile Phone">
									<input type="text" placeholder="Fax">
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="order-message">
							<p>Shipping Order</p>
							<textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
							<label><input type="checkbox"> Shipping to bill address</label>
						</div>	
					</div>					
				</div>
			</div>


			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>
			@endunless

			<div class="table-responsive cart_info">


				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>

						@if(isset($getSession))
							@foreach ($getSession as $row)
								<?php $img = $row['img']; ?>
								<tr>
									<td class="cart_product">
										<a href=""><img src="{{ asset('upload/product/'.'2'.$img) }}" alt=""></a>
									</td>
									<td class="cart_description">
										<h4><a href="">{{$row['name']}}</a></h4>
										<p>Web ID: {{$row['id']}}</p>
									</td>
									<td class="cart_price">
										<p>{{$row['price']}}</p>
									</td>
									<td class="cart_quantity">
										<div class="cart_quantity_button">
											<a class="cart_quantity_down" href="#"> - </a>
											<input class="cart_quantity_input" type="text" name="quantity" value="{{$row['qty']}}" autocomplete="off" size="2">
											<a class="cart_quantity_up" href="#"> + </a>
										</div>
									</td>
									<td class="cart_total">
										<p class="cart_total_price">{{$row['qty']*(int)$row['price']}} $</p>
									</td>
									<td class="cart_delete">
										<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
									</td>
								</tr>
							@endforeach

						@endif
						

						
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>
											$59
										</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td>
											<span class='cart_total_money'>
												<?php

												if(isset($getSession)){
													$_total_all = 0;
													foreach ($getSession as $row) {
														$_total_all = $_total_all + $row['qty']*(int)$row['price'];
													}
													echo $_total_all;
												} 
												
												?>
												
												 $
											</span>

										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
					<a href="{{ url('/sendemail') }}">Order</a>
				</table>

					
			</div>
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->

<script>
    $(document).ready(function(){
        
        

        $("a.cart_quantity_up").click(function(){

        	$.ajaxSetup({
            	headers: {
                	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            	}
        	});

            var cart=1
            var QTY = $(this).closest('tr').find('td.cart_quantity').find("input").val()
            QTY++;
            $(this).closest('tr').find('td.cart_quantity').find("input").attr("value",QTY)

            var Price = $(this).closest('tr').find('td.cart_price').find("p").text().replace('$','')
            Price =parseInt(Price)

            var ID_I  = $(this).closest('tr').find('td.cart__description').find('p').text().replace('Web ID:','').trim()
            var ID_P = $(this).closest('tr').find('td.cart_price').find("p").text()
            $(this).closest('tr').find('td.cart_total').find("p").text("$"+Price*QTY)
            var total = $("li.cart_total_money_1").find("span.cart_total_money").text().replace('$','')
            total =parseInt(total)
            $("span.cart_total_money").text("$"+ (total + Price))

            
     		$.ajax({
               type:'POST',
               url:"{{url('/member/cart/ajax')}}",
               data:{
                	QTY: QTY,
                    ID_I:ID_I,
                    cart:cart
			        },

               	success:function(QTY){
                  $('a#carthere').text(QTY);
               }

            });
        })
        
        $("a.cart_quantity_down").click(function(){

        	$.ajaxSetup({
            	headers: {
                	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            	}
        	});


            var cart=2
            var QTY = $(this).closest('tr').find('td.cart_quantity').find("input").val()
            QTY--
            if(QTY == 0){
                $(this).closest("tr").remove()
            }
            $(this).closest('tr').find('td.cart_quantity').find("input").attr("value",QTY)
            var Price = $(this).closest('tr').find('td.cart_price').find("p").text().replace('$','')
            Price =parseInt(Price)
            var ID_I  = $(this).closest('tr').find('td.cart__description').find("p").text().replace('Web ID:','').trim()

            var ID_P = $(this).closest('tr').find('td.cart_price').find("p").text()
            $(this).closest('tr').find('td.cart_total').find("p").text("$"+Price*QTY)
            var total = $("li.cart_total_money_1").find("span.cart_total_money").text().replace('$','')
            total =parseInt(total)
            $("li.cart_total_money_1").find("span.cart_total_money").text("$"+ (total - Price))
            
            $.ajax({
               type:'POST',
               url:"{{url('/member/cart/ajax')}}",
               data:{
                	QTY: QTY,
                    ID_I:ID_I,
                    cart:cart
			        },

               	success:function(QTY){
                  $('a#carthere').text(QTY);
               }

            });
        })

        $("a.cart_quantity_delete").click(function(){

        	$.ajaxSetup({
            	headers: {
                	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            	}
        	});

            var cart = 0;

            $(this).closest("tr").remove();

            var QTY = 0;

            var ID_I  = $(this).closest('tr').find('td.cart__description').find("p").text().replace('Web ID:','').trim();

            var QTY_2 = $(this).closest('tr').find('td.cart_quantity').find("input").val();

            var Price = $(this).closest('tr').find('td.cart_price').find("p").text().replace('$','');

            Price =parseInt(Price);

            var total = $("li.cart_total_money_1").find("span.cart_total_money").text().replace('$','');

            total =parseInt(total);
            
            $("li.cart_total_money_1").find("span.cart_total_money").text("$"+ (total - Price*QTY_2));
            
            $.ajax({
               type:'POST',
               url:"{{url('/member/cart/ajax')}}",
               data:{
                	QTY: QTY,
                    ID_I:ID_I,
                    cart:cart
			        },

               	success:function(QTY){
                  $('a#carthere').text(QTY);
               }

            });
        })

    })
</script>
@endsection