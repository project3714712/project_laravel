@extends('Frontend.Layouts.app')
@section('content2')

<script src="{{ asset('Frontend/js/jquery-1.9.1.min.js') }}"></script>


@foreach($data as $row)
<p class ="id_blog" style="display:none;">{{$row->id}} </p>
<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{$row->name}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Mac Doe</li>
									<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
									<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
								</ul>
								
							</div>
							<a href="">
								<img src="{{ url('Admin/ImageBlog/'.$row->image) }}" alt="">
							</a>
							<p>
								{{$row->description}}</p> <br>

							<p>
								{{$row->description}}</p> <br>

							<p>
								{{$row->description}}</p> <br>

							<p>
								{{$row->description}}
							</p>
@endforeach	
							<div class="pager-area">
								<ul class="pager pull-right">
									@if($previous)
									<li><a href="{{ URL( 'member/blog/single/' . $previous ) }}">Pre</a></li>
									@endif
									@if($next)
									<li><a href="{{ URL( 'member/blog/single/' . $next ) }}">Next</a></li>
									@endif
									
								</ul>
							</div>


						</div>
					</div><!--/blog-post-area-->

					<div class="rate">
                <div class="vote">
                    <div class="star_1 ratings_stars"><input value="1" type="hidden"></div>
                    <div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
                    <div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
                    <div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
                    <div class="star_5 ratings_stars"><input value="5" type="hidden"></div>
                    <span class="rate-np">{{$tbc_rate}}</span>
                </div> 
            </div>

					<div class="socials-share">
						<a href=""><img src="Frontend/images/blog/socials.png
							" alt=""></a>
					</div><!--/socials-share-->

					



					<div class="response-area">
						<h2>COMMENTS</h2>
						<ul class="media-list">
@foreach ($datacmt as $row)
	@if ($row->level == 0)
							<li class="media">
								<a class="pull-left" href="#">
									<img style="width: 100px; height: 100px" class="media-object" src="/Admin/imageuser/{{$row->avatar}}" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>{{$row->name}}</li>
										<li><i class="fa fa-clock-o"></i> {{$row->created_at}}</li>
										<!-- <li><i class="fa fa-calendar"></i> DEC 5, 2013</li> -->
									</ul>
									<p>{{$row->cmt}}</p>

									<a class="btn btn-primary reply" id="{{$row->id}}"><i class="fa fa-reply"></i>Replay</a>
								</div>
							</li>
	@endif	

    @foreach ($datacmt as $row2)
    	@if ($row2->level == $row->id)
    						 <li class="media second-media" id="replaybox">
								<a class="pull-left" href="#">
									<img style="width: 100px; height: 100px" class="media-object" src="/Admin/imageuser/{{$row2->avatar}}" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>{{$row2->name}}</li>
										<li><i class="fa fa-clock-o"></i>{{$row2->created_at}}</li>
										<!-- <li><i class="fa fa-calendar"></i> DEC 5, 2013</li> -->
									</ul>
									<p>{{$row2->cmt}}</p>
									<a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
								</div>
							</li> 
    @endif    
    @endforeach
@endforeach			
						</ul>					
					</div> 


					<div class="replay-box">
						<div class="row">
							<div class="col-sm-12">
								<h2>Leave a replay</h2>
								
								<div class="text-area">
									<form method="POST" action="{{ route('ajax_cmt') }}" id="myform" >
										@csrf
										<div class="blank-arrow">
											<label>Your Name</label>
										</div>
											<span>*</span>
											<textarea id="cmt" name="message" rows="11"></textarea>
											@foreach($data as $row)
											<input name="id_blog" value="{{$row->id}}" type="hidden">
											@endforeach
											<input name="level" class="level" value="0" type="hidden">
											<button type="submit" class="btn btn-primary comments" >post comment</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

<script>
    	
    	$(document).ready(function(){
			//vote
			$('.ratings_stars').hover(
	            // Handles the mouseover
	            function() {
	                $(this).prevAll().andSelf().addClass('ratings_hover');
	                // $(this).nextAll().removeClass('ratings_vote'); 
	            },
	            function() {
	                $(this).prevAll().andSelf().removeClass('ratings_hover');
	                // set_votes($(this).parent());
	            }
	        );

				$('.ratings_stars').click(function(){
					$.ajaxSetup({
					    headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    }
					});
					
			    	if ($(this).hasClass('ratings_over')) {
			            $('.ratings_stars').removeClass('ratings_over');
			            $(this).prevAll().andSelf().addClass('ratings_over');
			        } else  {
			        	$(this).prevAll().andSelf().addClass('ratings_over');
			        }  	      
					var CheckLog = "{{Auth::check()}}" ;
					var Values =  $(this).find("input").val();
					var id_blog = $("p.id_blog").text();
					
					if(CheckLog == "1"){
						    

						$.ajax({
	                		method: "POST",
	                		url: "{{url('/blog/rate/ajax')}}",
			                data: {
			                    Values: Values,
			                    id_blog:id_blog,
			                    
			                },
			                success : function(output){
			                              	
			                }
	            		})
					
					}
					else{
						alert("Xin hãy đăng nhập");
					}
			 });

			$('a.reply').click(function(){
				var getId = $(this).attr("id");
				alert(getId)
				$('input.level').val(getId);
			})

    		$("form").submit(function(){
    			var CheckLog = "{{Auth::check()}}";
    			if(CheckLog != "1"){
						alert("Xin hãy đăng nhập");
						return false;
					}

				let element = document.getElementById('cmt');
    			if(element.value == ""){
    					alert("Xin hãy nhập bình luận");
						return false;
    			}
    			return true;
    		})
		});
    </script>
@endsection