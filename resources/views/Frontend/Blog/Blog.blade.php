@extends('Frontend.Layouts.app')
@section('content2')
<div class="col-sm-9">

	@foreach($data as $row)
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{$row->name}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Mac Doe</li>
									<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
									<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
								</ul>
								<span>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
								</span>
							</div>
							<p style="display:none;">{{$id =$row->id}}</p>
							<a href="{{ url('member/blog/single/'.$id) }}">
								<img src="{{ url('Admin/ImageBlog/'.$row->image) }}" alt="" />
							</a>

							
							
							<p>{{$row->description}}</p>
							<a  class="btn btn-primary" href="{{ url('member/blog/single/'.$id) }}">Read More</a>
						</div>

	@endforeach
						<div class="pagination-area">
							<ul class="pagination">
								{{$data->links('pagination::bootstrap-4')}}
							</ul>
						</div>
					</div>
				</div>
@endsection