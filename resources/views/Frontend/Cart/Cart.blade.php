@extends('Frontend.Layouts.app2')
@section('content3')

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Image</td>
							<td class="description">Name</td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td class="total">Action</td>
						</tr>
					</thead>
					<tbody>
						@foreach ($getSession as $row)
							<?php $img = $row['img'];
								
							?>
							<tr>
                                    <td class='cart_image'>
                                        <a><img src="{{ asset('upload/product/'.'2'.$img) }}"alt=''></a>
                                    </td>
                                    <td class='cart__description'>
                                        <h4><a>{{$row['name']}}</a></h4>
                                        <p>Web ID: {{$row['id']}}</p>
                                    </td>
                                    <td class='cart_price'>
                                        <p>{{$row['price']}} $</p>
                                    </td>
                                    <td class= 'cart_quantity'>
                                        <div class= 'cart_quantity_button'>
                                            <a  class= 'cart_quantity_down' > - </a>
                                            <input class='cart_quantity_input' type='text' name='quantity' value="{{$row['qty']}}" autocomplete='off' size='2'>
                                            <a  class= 'cart_quantity_up' > + </a>
                                        </div>
                                    </td>
                                    <td class='cart_total'>
                                        <p class='cart_total_price'> {{$row['qty']*(int)$row['price']}} $</p>
                                    </td> 
                                    	<td class='cart_delete'> 
                                        <a  class='cart_quantity_delete' ><i class='fa fa-times'></i></a>
                                    </td>                            
                        	</tr>
                        	
                        @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section> 

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li >Cart Sub Total <span>$</span></li>
							<li class="tax">Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li class = "cart_total_money_1">Total <span class = "cart_total_money">

							<?php 
								$_total_all = 0;
								foreach ($getSession as $row) {
									$_total_all = $_total_all + $row['qty']*(int)$row['price'];
								}
								echo $_total_all;
							?>
							 	$
							 </span></li>
						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->

<script>
    $(document).ready(function(){
        
        

        $("a.cart_quantity_up").click(function(){

        	$.ajaxSetup({
            	headers: {
                	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            	}
        	});

            var cart=1
            var QTY = $(this).closest('tr').find('td.cart_quantity').find("input").val()
            QTY++;
            $(this).closest('tr').find('td.cart_quantity').find("input").attr("value",QTY)

            var Price = $(this).closest('tr').find('td.cart_price').find("p").text().replace('$','')
            Price =parseInt(Price)

            var ID_I  = $(this).closest('tr').find('td.cart__description').find('p').text().replace('Web ID:','').trim()
            var ID_P = $(this).closest('tr').find('td.cart_price').find("p").text()
            $(this).closest('tr').find('td.cart_total').find("p").text("$"+Price*QTY)
            var total = $("li.cart_total_money_1").find("span.cart_total_money").text().replace('$','')
            total =parseInt(total)
            $("li.cart_total_money_1").find("span.cart_total_money").text("$"+ (total + Price))

            
     		$.ajax({
               type:'POST',
               url:"{{url('/member/cart/ajax')}}",
               data:{
                	QTY: QTY,
                    ID_I:ID_I,
                    cart:cart
			        },

               	success:function(QTY){
                  $('a#carthere').text(QTY);
               }

            });
        })
        
        $("a.cart_quantity_down").click(function(){

        	$.ajaxSetup({
            	headers: {
                	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            	}
        	});


            var cart=2
            var QTY = $(this).closest('tr').find('td.cart_quantity').find("input").val()
            QTY--
            if(QTY == 0){
                $(this).closest("tr").remove()
            }
            $(this).closest('tr').find('td.cart_quantity').find("input").attr("value",QTY)
            var Price = $(this).closest('tr').find('td.cart_price').find("p").text().replace('$','')
            Price =parseInt(Price)
            var ID_I  = $(this).closest('tr').find('td.cart__description').find("p").text().replace('Web ID:','').trim()

            var ID_P = $(this).closest('tr').find('td.cart_price').find("p").text()
            $(this).closest('tr').find('td.cart_total').find("p").text("$"+Price*QTY)
            var total = $("li.cart_total_money_1").find("span.cart_total_money").text().replace('$','')
            total =parseInt(total)
            $("li.cart_total_money_1").find("span.cart_total_money").text("$"+ (total - Price))
            
            $.ajax({
               type:'POST',
               url:"{{url('/member/cart/ajax')}}",
               data:{
                	QTY: QTY,
                    ID_I:ID_I,
                    cart:cart
			        },

               	success:function(QTY){
                  $('a#carthere').text(QTY);
               }

            });
        })

        $("a.cart_quantity_delete").click(function(){

        	$.ajaxSetup({
            	headers: {
                	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            	}
        	});

            var cart = 0;

            $(this).closest("tr").remove();

            var QTY = 0;

            var ID_I  = $(this).closest('tr').find('td.cart__description').find("p").text().replace('Web ID:','').trim();

            var QTY_2 = $(this).closest('tr').find('td.cart_quantity').find("input").val();

            var Price = $(this).closest('tr').find('td.cart_price').find("p").text().replace('$','');

            Price =parseInt(Price);

            var total = $("li.cart_total_money_1").find("span.cart_total_money").text().replace('$','');

            total =parseInt(total);
            
            $("li.cart_total_money_1").find("span.cart_total_money").text("$"+ (total - Price*QTY_2));
            
            $.ajax({
               type:'POST',
               url:"{{url('/member/cart/ajax')}}",
               data:{
                	QTY: QTY,
                    ID_I:ID_I,
                    cart:cart
			        },

               	success:function(QTY){
                  $('a#carthere').text(QTY);
               }

            });
        })

    })
</script>
@endsection