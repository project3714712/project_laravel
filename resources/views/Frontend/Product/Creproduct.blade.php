@extends('Frontend.Layouts.app2')
@section('content3')
    <div class="col-sm-4">
            <div class="mainmenu pull-left">
                    <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{ url('member/account') }}">Account</a></li>
                          
                            <li><a href="{{ url('member/product') }}">My Products</a></li>
                    </ul>
            </div>  
        </div>

        <div class="col-sm-8">
            <div class="signup-form" style="">
					<h2>Create Product !</h2>
						<form action="{{ route('add_pr') }}" method="POST" enctype="multipart/form-data">
							@csrf

                            <input type="text" style="display: none;" value="{{ Auth::user()->id }}" name="id_user">

							<input type="text" name="Name" value="" placeholder="Name">

							<input type="text" name="Price" value="" placeholder="Price">

                            <select class="form-control form-control-line" name="Category" placeholder="Select Category">
                            	<option>Select Category</option> 
                            	@foreach($data as $row)            
                                <option value="{{$row->name}}">{{$row->name}}</option>
                                @endforeach                               
                            </select>
                                     
                            <select class="form-control form-control-line" name="Brand" placeholder="Select Brand">  
                            	<option>Select Brand</option>          
                                @foreach($data2 as $row)            
                                <option value="{{$row->name}}">{{$row->name}}</option>
                                @endforeach   
                            </select>

                            <select class="form-control form-control-line" id='selector' name="Sale">            
                                <option>Status</option>
                                <option value="0">New</option>
                                <option value="1">Sale</option>

                            </select>

                            <input class="colors" style="display: none;" type="text" name="Sale2" id="sale2" value="" placeholder="0%">

                            <input type="text" name="Company" value="" placeholder="Company Profile">

                            <input multiple type="file" class="form-control form-control-line" name="filename[]" >
                                
							<textarea cols="30" rows="7" name="message" placeholder="Detail Product"></textarea>
							<button type="submit" class="btn btn-default">OK</button>
						</form>
			</div>
        </div>



<script language="javascript">
 

$(function() {
    $('#selector').change(function(){
        $('.colors').hide();

        var hd = $('option').closest('#selector').val();

        console.log(hd)
        if(hd == 1){
            $('.colors').show();
        }
        
    });
});
            
        
</script>
@endsection