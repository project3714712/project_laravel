@extends('Frontend.Layouts.app2')
@section('content3')
    <div class="col-sm-3">
            <div class="mainmenu pull-left">
                    <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{ url('member/account') }}">Account</a></li>
                          
                            <li><a href="{{ url('member/product') }}">My Products</a></li>
                    </ul>
            </div>  
        </div>

        <div class="col-sm-9">
                                
            <section id="cart_items">
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">ID</td>
                            <td class="description">Name</td>
                            <td class="quantity">Image</td>
                            <td class="price">Price</td>
                            <td class="total">Action</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($getProducts as $row)
                           <?php $img = json_decode($row['img'], true); ?>
                                {{$id = $row['id']}}
                                <tr>
                                    <td>{{$row['id']}}</td>
                                    <td>{{$row['name']}}</td>
                                    <td> <img src="{{ asset('upload/product/'.'2'.$img[0]) }}"> </td>
                                    <td>{{$row['price']}}</td>
                                    <td><a href="{{ url('member/product/edit/'.$id) }}">Edit</a></td>
                                    <td><a href="#">Delete</a></td>
                                </tr>
                            
                        @endforeach

                    </tbody>
                </table>
            </div>
                <a  href="{{ url('member/product/add') }}" class="btn btn-default">ADD</a>
            </section> 
        </div>



@endsection
