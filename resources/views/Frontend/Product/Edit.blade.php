@extends('Frontend.Layouts.app2')
@section('content3')
    <div class="col-sm-4">
            <div class="mainmenu pull-left">
                    <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="{{ url('member/account') }}">Account</a></li>
                          
                            <li><a href="{{ url('member/product') }}">My Products</a></li>
                    </ul>
            </div>  
        </div>

        <div class="col-sm-8">
            @if(session('success'))
                                <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i>Thông báo</h4>
                                {{session('success')}}
                                </div>
                                @endif

                                @if(session('error'))
                                <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <h4><i class="icon fa fa-check"></i> Thông báo!<in4>
                                    {{session('error')}}
                                </div>
                                @endif
            <div class="signup-form" style="">
                    <h2>Edit Product !</h2>

                        @foreach($getProducts as $row)
                        <form action="{{ url('member/product/edit/$row->id') }}" method="POST" enctype="multipart/form-data">
                            
                            @csrf

                            <input style="display: none;" type="text" name="id" value="{{$row['id']}}" placeholder="Name">

                            <input type="text" name="Name" value="{{$row['name']}}" placeholder="Name">

                            <input type="text" name="Price" value="{{$row['price']}}" placeholder="Price">

                             

                            <select class="form-control form-control-line" name="Category" placeholder="Select Category">    
                                <option value="{{$row['category']}}">{{$row['category']}}</option>
                                @foreach($data as $row1)            
                                <option value="{{$row1->name}}">{{$row1->name}}</option>
                                @endforeach                            
                            </select>
                            @endforeach

                            @foreach($getProducts as $row)       
                            <select class="form-control form-control-line" name="Brand" placeholder="Select Brand">                    
                                <option value="{{$row['brand']}}">{{$row['brand']}}</option>
                                 @foreach($data2 as $row2)            
                                <option value="{{$row2->name}}">{{$row2->name}}</option>
                                @endforeach           
                            </select>
                            
                            <select class="form-control form-control-line" id='selector' name="Sale">            
                                <option>Status</option>
                                <option value="0">New</option>
                                <option value="1">Sale</option>

                            </select>

                            
                            <input class="colors" style="display: none;" type="text" name="Sale2" id="sale2" value="{{$row['sale']}}" placeholder="0%">

                            <input type="text" name="Company" value="{{$row['company']}}" placeholder="Company Profile">

                            <input multiple type="file" class="form-control form-control-line" name="filename[]" >
                            
                            <?php $img = json_decode($row['img'], true); ?>

                            @foreach($img as $row3) 
                            <img for="checkimg" src="{{ asset('upload/product/'.'2'.$row3) }}"> 
                            <input type="checkbox" id="vehicle1" name="checkimg[]" value="<?php echo $row3 ?>">
                            @endforeach

                            <textarea cols="30" rows="7" name="message" placeholder="Detail Product">{{$row['detail']}}</textarea>

                            @endforeach
                            <button type="submit" class="btn btn-default">OK</button>
                        </form>
            </div>
        </div>


<script language="javascript">
 

$(function() {
    $('#selector').change(function(){
        $('.colors').hide();

        var hd = $('option').closest('#selector').val();

        console.log(hd)
        if(hd == 1){
            $('.colors').show();
        }
        
    });
});
            
        
</script>
@endsection