




	<section id="cart_items">
		<div class="container">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>

						<?php  if(session()->has('cart')){
            					$getSession = session()->get('cart');
        				} ?>

							@foreach ($getSession as $row)
								<?php $img = $row['img']; ?>
								<tr>
									<td class="cart_product">
										<a href=""><img src="{{ asset('upload/product/'.'2'.$img) }}" alt=""></a>
									</td>
									<td class="cart_description">
										<h4><a href="">{{$row['name']}}</a></h4>
										<p>Web ID: {{$row['id']}}</p>
									</td>
									<td class="cart_price">
										<p>{{$row['price']}}</p>
									</td>
									<td class="cart_quantity">
										<div class="cart_quantity_button">
											<a class="cart_quantity_down" href="#"> - </a>
											<input class="cart_quantity_input" type="text" name="quantity" value="{{$row['qty']}}" autocomplete="off" size="2">
											<a class="cart_quantity_up" href="#"> + </a>
										</div>
									</td>
									<td class="cart_total">
										<p class="cart_total_price">{{$row['qty']*(int)$row['price']}} $</p>
									</td>
									<td class="cart_delete">
										<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
									</td>
								</tr>
							@endforeach

						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>
											$59
										</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td>
											<span class='cart_total_money'>
												<?php

												if(isset($getSession)){
													$_total_all = 0;
													foreach ($getSession as $row) {
														$_total_all = $_total_all + $row['qty']*(int)$row['price'];
													}
													echo $_total_all;
												} 
												
												?>
												
												 $
											</span>

										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>

				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->


