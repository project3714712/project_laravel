<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>

	<table>
		
		Hi, {{ $name }}
			Your Cart order here :

			| ID | Name | Price | Total |
			@if(isset($body))
			@foreach ($body as $row)
			{{$row['id']}} | {{$row['name']}} | {{$row['price']}} |{{$row['qty']*(int)$row['price']}}
			@endforeach
			@endif

	</table>

</body>
</html>