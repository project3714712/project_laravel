<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MembercheckLoginAuthetication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request)
    {
        if( Auth::check() ){
            return redirect('member/dashboard');
        }else{
            return redirect()->back()->withErrors('Thao tác không đúng quy trình! Xin thử lại!');
        }
    }
}

