<?php

namespace App\Http\Controllers\Admin;
use App\Models\Brand;
use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BrandandcategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$data = Category::all();
        return view ('Admin.Category.Category', compact('data'));
    }

    public function index2()
    {
    	$data = Brand::all();
        return view ('Admin.Brand.Brand', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Category.Action.add');
    }

    public function create2()
    {
        return view('Admin.Brand.Action.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $data = Category::insert([
            'name' => $request->title,
            ]);
        return redirect()->action([BrandandcategoryController::class,'index']);
    }

    public function store2(Request $request)
    {
        $data = Brand::insert([
            'name' => $request->title,
            ]);
        return redirect()->action([BrandandcategoryController::class,'index2']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Category::where('id',$id)->get();
        return view('Admin.Category.Action.edit', compact('data'));
    }

    public function edit2($id)
    {
        $data = Brand::where('id',$id)->get();
        return view('Admin.Brand.Action.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Category::where('id',$id)->update([
        'name' => $request->title,
    ]);
        return redirect()->action([BrandandcategoryController::class,'index']);
    }

    public function update2(Request $request, $id)
    {
        $data = Brand::where('id',$id)->update([
        'name' => $request->title,
    ]);
        return redirect()->action([BrandandcategoryController::class,'index2']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Category::where('id',$id) ->delete();
        return redirect()->action([BrandandcategoryController::class,'index']);
    }

    public function destroy2($id)
    {
        $data = Brand::where('id',$id) ->delete();
        return redirect()->action([BrandandcategoryController::class,'index2']);
    }
}
