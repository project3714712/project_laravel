<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Country;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $data = Country::all();
        return view('Admin.Profile.Profile', compact('data'));
    }

    public function update(ProfileUpdateRequest $request)
    {
    

        $userId = Auth::id();
        $user = User::findOrFail($userId);

        $data = $request->all();

        if($data['pass']){
            $data['pass'] = bcrypt($data['pass']);
        }else{
            $data['pass']= $user->password;
        }

        if($request->hasFile('filesTest')){
            $file = $request->file('filesTest');

            // //Lấy Tên files
            // echo 'Tên Files: ' . $file->getClientOriginalName();
            // echo '<br/>';

            // //Lấy Đuôi File
            // echo 'Đuôi file: ' . $file->getClientOriginalExtension();
            // echo '<br/>';

            // //Lấy đường dẫn tạm thời của file
            // echo 'Đường dẫn tạm: ' . $file->getRealPath();
            // echo '<br/>';

            // //Lấy kích cỡ của file đơn vị tính theo bytes
            // echo 'Kích cỡ file: ' . $file->getSize();
            // echo '<br/>';

            // //Lấy kiểu file
            // echo 'Kiểu files: ' . $file->getMimeType();
            $data['avatar'] = $file->getClientOriginalName();
               
        }

        if($user->update($data)){
            if(!empty($file)){
               $file->move('Admin/imageuser',$file->getClientOriginalName());
               
            }
            
            return redirect()->back()->with('success', __('Update profile success. '));
        }else{
            return redirect()->back()->withErrors('Update profile error');
        }

        
        
    }



}
