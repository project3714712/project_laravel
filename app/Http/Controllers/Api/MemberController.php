<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view ('Frontend.Member.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $file = $request->file('filesimg');
        $file->move('Admin/imageuser',$file->getClientOriginalName());         
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'avatar'=> $file->getClientOriginalName(),
            'address' => $request['address'],
            'password' => Hash::make($request['pass']),
            'level' => 0,
        ]);
        return redirect('member/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return view ('Frontend.Member.login');
    }

    public function login(Request $request)
    {
        $login = [
            'email' => $request['email'],
            'password' => $request['password'],
            'level' => 0
        ];
        

        $remember = false;

        if ($request->remember_me) {
            $remember = true;
        }

        if (Auth::attempt($login,$remember)){
            
            return response()->json([
            'response' => 'success',
            'data' => Auth::id(),
        ]);
            
        }
        else {
         return redirect()->back()->withErrors('Email or password is not correct.');
        }

    }

    public function dashboard()
    {
        $getProducts = Product::select('*')->orderBy('created_at', 'DESC')->take(6)->get()->toArray();

        return view ('Frontend.index', compact('getProducts'));
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
