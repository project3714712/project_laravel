<?php
namespace App\Http\Controllers\Api;
use App\Models\User;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public $successStatus = 200;

    public function index()
    {
        // $id_user = Auth::id();
        $getProducts = Product::all()->toArray();

    	return response()->json([
            'response' => 'success',
            'data' => $getProducts
        ], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$data = Category::all();
    	$data2 = Brand::all();
        return view ('Frontend.Product.Creproduct', compact('data', 'data2'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Category::all();
        $data2 = Brand::all();
        $getProducts = Product::where('id',$id)->get()->toArray();
        return view ('Frontend.Product.Edit', compact('getProducts', 'data', 'data2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo 123;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rqimg=[];

        $getProducts = Product::where('id',$request->id)->get()->toArray();

        $img = json_decode($getProducts[0]['img'], true);

        $rqimg=$request->checkimg;
        
        if($request->checkimg){
            $rqimg=$request->checkimg;
            foreach ($img as $row=> $value) {
                if(in_array($value , $rqimg)){
                    unset($img[$row]);
                }
            }
        }
        
        $img=array_values($img);    
        
        if($request->hasFile('filename')){
            foreach($request->file('filename') as $image)
            {

                $name = $image->getClientOriginalName();
                $name_2 = "2".$image->getClientOriginalName();
                $name_3 = "3".$image->getClientOriginalName();

                //$image->move('upload/product/', $name);
                
                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name_2);
                $path3 = public_path('upload/product/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(50, 70)->save($path2);
                Image::make($image->getRealPath())->resize(200, 300)->save($path3);
                
                $data[] = $name;
                
                $result = array_merge($data, $img);
                
            }
        }else{
            $result = $img;
        }

        $count = count($result);

        if($count > 3){
            return redirect()->back()->with('error', __('Hình ảnh vượt quá 3. '));
        }

        // dd($result);


        Product::where('id',$request->id)->update([
                'name' => $request->Name,
                'price' => $request->Price,
                'category' => $request->Category,
                'brand' => $request->Brand,
                'state' => $request->Sale,
                'sale' =>  $request->Sale2,
                'company'=> $request->Company,
                'img' => json_encode($result),
                'detail' => $request->message,
                'updated_at' => Carbon::now()->hour()->minute(),
                ]);
        

        return redirect()->back()->with('success',  __('Cập nhật thành công. '));

    }

    public function viewdetail($id)
    {
        $getProducts = Product::where('id',$id)->get()->toArray();
        return view ('Frontend.Product.Productdetail', compact('getProducts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
