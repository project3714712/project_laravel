<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Rate;
use App\Models\Cmt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class BlogController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Blog::paginate(2);
        // return view ('Frontend.Blog.Blog',compact('data'));
        return view('Frontend.Blog.Blog', [
            'data' => Blog::paginate(2)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Blog::where('id',$id)->get();

        $blog = Blog::find($id);

        $previous = Blog::where('id', '<', $blog->id)->max('id');
        $next = Blog::where('id', '>', $blog->id)->min('id');

        $datarate = Rate::where('id_blog', $id)->get();
        $tbc_rate = Rate::select('rate')->where('id_blog',$id)->avg('rate');
        $tbc_rate =round($tbc_rate);

        $datacmt = Cmt::where('id_blog', $id)->get();

        

            return view ('Frontend.Blog.Blogdetail.Blogdetail',compact('data', 'datarate', 'previous', 'next' , 'tbc_rate' ,'datacmt'));
    }

    public function ajax(Request $request)
    {
        $output= $request->Values;
        if ($request->ajax()) {
                Rate::insert([
                'rate' => $request->Values,
                'id_blog' => $request->id_blog,
                'id_user' => Auth::id(),
                ]);
            }

        return Response($output);
    }


    public function Comment(Request $request)
    {   
            
                 Cmt::insert([
                'name' => Auth::user()->name,
                'id_blog' => $request->id_blog,
                'id_user' => Auth::id(),
                'avatar' => Auth::user()->avatar,
                'level'  => $request->level,
                'cmt' => $request->message,
                'created_at' => Carbon::now()->hour()->minute()
                ]);

                // $cmtson = Cmt::where('id', $request->level)->get();

        return redirect()->back()->with('success',  __('Comments success. '));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
