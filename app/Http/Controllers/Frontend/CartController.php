<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
class CartController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->has('cart')) {
            $getSession = session()->get('cart');
        }
        return view ('Frontend.Cart.Cart', compact('getSession'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    public function addtocart(Request $request)
    {
        $id= $request->getID;
     	$id=(int)$id;
        $getProducts = Product::where('id',$id)->get()->toArray();

        $img = json_decode($getProducts[0]['img'], true);

        $array =[];
        $array['id']= $id;
        $array['name']= $getProducts[0]['name'];
        $array['price']= $getProducts[0]['price'];
        $array['img'] = $img[0];
        $array['qty'] = 1;

        if(session()->has('cart')) {
            
        	$getSession = session()->get('cart');
        	$flag = 1;
        	foreach ($getSession as $key => $value) {
        		if($id == $value['id']){
        			$getSession[$key]['qty'] +=1;
        			session()->put('cart' , $getSession);
        			$flag = 0;
        			break;
        		}
        	}

        	if($flag == 1){
        		session()->push('cart', $array);
        	}

        } else {
        	session()->push('cart', $array);
        }

        if (session()->has('cart')) {
            $QTY = 0;
            $getSession = session()->get('cart');

            foreach ($getSession as $key => $value) {
                $QTY = $getSession[$key]['qty'] + $QTY;
            }
            
        }

        return Response($QTY.' '.'In Cart');

    }


    public function updatecart(Request $request)
    {   
        $id = $request->ID_I;

        $qty = $request->QTY;

        $cart = $request->cart;

        if($cart==1){

            if(session()->has('cart')) {
                $getSession = session()->get('cart');
                
                foreach ($getSession as $key => $value) {
                    if($value['id'] == $id ){
                        $getSession[$key]['qty'] = $qty;
                        session()->put('cart' , $getSession);  
                    }
                }
            
            }

            if (session()->has('cart')) {
                $QTY = 0;
                $getSession = session()->get('cart');

                foreach ($getSession as $key => $value) {
                    $QTY = $getSession[$key]['qty'] + $QTY;
                }
                
            }

            return Response($QTY.' '.'In Cart');

        }

        if($cart==2){

            if(session()->has('cart')) {
                $getSession = session()->get('cart');
                
                foreach ($getSession as $key => $value) {
                    if($value['id'] == $id ){
                        $getSession[$key]['qty'] = $qty;

                        if($getSession[$key]['qty'] == 0){
                            unset($getSession[$key]);
                        }
                        session()->put('cart' , $getSession);  
                    }
                }
            
            }

            if (session()->has('cart')) {
                $QTY = 0;
                $getSession = session()->get('cart');

                foreach ($getSession as $key => $value) {
                    $QTY = $getSession[$key]['qty'] + $QTY;
                }
                
            }

            return Response($QTY.' '.'In Cart');
        }
        

        if($cart == 0){

            if(session()->has('cart')) {
                $getSession = session()->get('cart');
                
                foreach ($getSession as $key => $value) {
                    if($value['id'] == $id ){
                        $getSession[$key]['qty'] = 0;
                        unset($getSession[$key]);
                        session()->put('cart' , $getSession);  
                    }
                }
            }

            if (session()->has('cart')) {
                $QTY = 0;
                $getSession = session()->get('cart');

                foreach ($getSession as $key => $value) {
                    $QTY = $getSession[$key]['qty'] + $QTY;
                }
                
            }

            return Response($QTY.' '.'In Cart');
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
