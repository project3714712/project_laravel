<?php
namespace App\Http\Controllers\Frontend;
use App\Models\User;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index(Request $request)
    {	

    	if (!empty($request->price)) {
            switch ($request->price) {

                case 1:
                    $min=100;
                    $max=200;
                    break;
                case 2:
                    $min=200;
                    $max=300;
                    break;
                case 3:
                    $min=300;
                    $max=400;
                    break;
                case 4:
                    $min=400;
                    $max=500;
                    break;
                case 5:
                    $min=500;
                    $max=600;
                    break;
                default:
                    $min=Product::min('price');
                    $max=Product::max('price');
                    break;
            }
        }


        $getProducts = Product::query();

        if (!empty($request->search)) {
            $getProducts = Product::where('name','LIKE',"%{$request->search}%");
        }

        if (!empty($request->price)) {
            $getProducts=$getProducts->where('price','>=',$min)->where('price','<=',$max);
        }

        $getProducts=$getProducts->get();

    	return view ('Frontend.Search.Search',compact('getProducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function searchajax(Request $request)
    {
        $price = $request->price;

        $price = explode(':', $price);

        $min = $price[0];
        $max = $price[1];
        $min = trim($min);
        $max = trim($max);
        
        $getProducts = Product::whereBetween('price', [$min, $max])->get()->toArray();
        
        // dd($getProducts);

        return response()->json(['data' => $getProducts]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    
    	//    
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
