<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Mail\MailNotify;
use App\Http\Requests;
use App\Models\History;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class MailController extends Controller {


    public function basic_email() {

        if(session()->has('cart')){
            $getSession = session()->get('cart');
        }

          $data = array('name'=>Auth::user()->name,
                        'body'=>$getSession,
                        );
       
          Mail::send(['text'=>'mail'], $data, function($message) 
          {
             $message->to('14dinepaytion@gmail.com', 'Tutorials Point')->subject
                ('Laravel Basic Testing Mail');
             $message->from('43.dibpaytion@gmail.com','Cart');
          });
          echo "Basic Email Sent. Check your inbox.";
   }


   public function index()
   {
        
        if(session()->has('cart')){
            $_total_all = 0;
            $getSession = session()->get('cart');
                foreach ($getSession as $row) {
                    $_total_all = $_total_all + $row['qty']*(int)$row['price'];
                }
        }

        

        History::insert([
                'email' => Auth::user()->email,
                'name' => Auth::user()->name,
                'phone' => Auth::user()->phone,
                'id_user' => Auth::id(),
                'price' => $_total_all,
                'created_at' => Carbon::now()->hour()->minute()
                ]);

     $data = [
        'subject' => 'Cambo mail',
        'body' => 'hello this is mail',

     ];

     try {
        Mail::to('14dinepaytion@gmail.com')->send(new MailNotify($data));
        return response()->json(['Check mail box']);
     }catch (Exception $th){
        return response()->json(['Wrong something']);
     }
   }




   // public function html_email() {
   //    $data = array('name'=>"Virat Gandhi");
   //    Mail::send('mail', $data, function($message) {
   //       $message->to('abc@gmail.com', 'Tutorials Point')->subject
   //          ('Laravel HTML Testing Mail');
   //       $message->from('xyz@gmail.com','Virat Gandhi');
   //    });
   //    echo "HTML Email Sent. Check your inbox.";
   // }
   // public function attachment_email() {
   //    $data = array('name'=>"Virat Gandhi");
   //    Mail::send('mail', $data, function($message) {
   //       $message->to('abc@gmail.com', 'Tutorials Point')->subject
   //          ('Laravel Testing Mail with Attachment');
   //       $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
   //       $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
   //       $message->from('xyz@gmail.com','Virat Gandhi');
   //    });
   //    echo "Email Sent with attachment. Check your inbox.";
   // }
}






