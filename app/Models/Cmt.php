<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cmt extends Model
{
    // use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'comments';
    public $timestamps = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $fillable = [
        'name',
        'avatar',
        'cmt',
        'id_blog',
        'id_user',
        'level',
        'created_at'
    ];
}
