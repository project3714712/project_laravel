<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    // use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'history';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $fillable = [
        'email',
        'phone',
        'name',
        'price',
        'id_user',
        'created_at'
    ];
}
