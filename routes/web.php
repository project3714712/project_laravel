<?php
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Frontend\MemberController;
use App\Http\Controllers\Frontend\AccountController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\Admin\BrandandcategoryController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Frontend\SearchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/sendbasicemail', [MailController::class, 'basic_email']);




Route::group([
        'prefix' => 'member',
        // 'middleware' => ['MembercheckLoginAuthetication'],
        
], function (){
    Route::get('/register', [MemberController::class, 'index']);
    Route::post('/register', [MemberController::class, 'create'])->name('memberreg');

    Route::get('/login', [MemberController::class, 'store']); 
    Route::post('/login', [MemberController::class, 'login'])->name('memberlogin');

    Route::get('/dashboard', [MemberController::class, 'dashboard']);
    Route::get('/blog', [App\Http\Controllers\Frontend\BlogController::class, 'index'])->name('bloglist');

    Route::get('/blog/single/{id}', [App\Http\Controllers\Frontend\BlogController::class, 'show'])->name('detailblog');

    
});

    //BLOG 
    Route::post('/blog/rate/ajax', [App\Http\Controllers\Frontend\BlogController::class, 'ajax'])->name('ajax_rate');

    Route::post('/blog/cmt/hihi', [App\Http\Controllers\Frontend\BlogController::class, 'Comment'])->name('ajax_cmt');



    //MAIL

    // Route::get('/send', [MailController::class, 'index']);
    
    Route::get('/sendemail', [MailController::class, 'index'])->name('sendemail');


    //SEARCH

    Route::get('/search', [SearchController::class, 'index'])->name('search');
    Route::post('/search/ajaxRequest', [SearchController::class, 'searchajax']);
    



Route::group([
    'prefix' => 'member', 
    // 'middleware' => ['member'],
], function () {
    //ACOUNT
    Route::get('/account', [AccountController::class, 'index'])->middleware(['auth']);

    Route::post('/account', [AccountController::class, 'update'])->middleware(['auth'])->name('account_update');

    //PRODUCT

    Route::get('/product', [ProductController::class, 'index']);

    Route::get('/product/add', [ProductController::class, 'create'])->middleware(['auth']);

    Route::post('/product/add', [ProductController::class, 'store'])->middleware(['auth'])->name('add_pr');

    Route::get('/product/edit/{id}', [ProductController::class, 'show'])->middleware(['auth']);
    Route::post('/product/edit/{id}', [ProductController::class, 'update'])->middleware(['auth'])->name('edit_pr');

    Route::get('/product/detail/{id}', [ProductController::class, 'viewdetail'])->middleware(['auth']);

    //CART

    Route::post('/cart/ajaxRequest', [CartController::class, 'addtocart'])->middleware(['auth']);

    Route::get('/cart', [CartController::class, 'index'])->middleware(['auth']);


    Route::post('/cart/ajax', [CartController::class, 'updatecart'])->middleware(['auth']);



    //CHECKOUT

    Route::get('/checkout', [CheckoutController::class, 'index']);
});













// -----------------------------------------------------------------------------------------------------------------------
Auth::routes();


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');






Route::group([
    'prefix' => 'admin',
    'namespace' => 'Auth'
], function () {
    Route::get('/',[LoginController::class, 'showLoginForm']);
    Route::get('/login',[LoginController::class, 'showLoginForm']);
    Route::post('/login',[LoginController::class, 'login']);
    Route::get('/logout',[LoginController::class, 'logout']);
});




Route::group([
    'prefix' => 'admin', 
    'namespace' => 'Admin',
    // 'middleware' => ['admin'],
], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->middleware(['auth', 'verified'])->name('dashboard');

    // PROFILE
    Route::get('/Profile', [UserController::class, 'index'])->middleware(['auth']);
    Route::post('/Profile', [UserController::class, 'update'])->middleware(['auth'])->name('upload.handle');

    // COUNTRY
    Route::get('/Country', [App\Http\Controllers\Admin\CountryController::class, 'index'])->middleware(['auth']);

    Route::get('/Country/Add', [App\Http\Controllers\Admin\CountryController::class, 'create'])->middleware(['auth']);
    Route::post('/Country/Add', [App\Http\Controllers\Admin\CountryController::class, 'store'])->middleware(['auth'])->name('add.handle');

    Route::get('/Country/Edit/{id}', [App\Http\Controllers\Admin\CountryController::class, 'edit'])->middleware(['auth']);
    Route::post('/Country/Edit/{id}', [App\Http\Controllers\Admin\CountryController::class, 'update'])->middleware(['auth']);

    Route::get('/Country/Delete/{id}', [App\Http\Controllers\Admin\CountryController::class, 'destroy'])->middleware(['auth']);


    //BLOG
    Route::get('/Blog', [App\Http\Controllers\Admin\BlogController::class, 'index'])->middleware(['auth']);
    Route::get('/Blog/Add', [App\Http\Controllers\Admin\BlogController::class, 'create'])->middleware(['auth']);
    Route::post('/Blog/Add', [App\Http\Controllers\Admin\BlogController::class, 'store'])->middleware(['auth'])->name('addblog.handle');

    Route::get('/Blog/Edit/{id}', [App\Http\Controllers\Admin\BlogController::class, 'edit'])->middleware(['auth']);
    Route::post('/Blog/Edit/{id}', [App\Http\Controllers\Admin\BlogController::class, 'update'])->middleware(['auth']);

    Route::get('/Blog/Delete/{id}', [App\Http\Controllers\Admin\BlogController::class, 'destroy'])->middleware(['auth']);


    //BRAND&CATEGORY
    Route::get('/category', [BrandandcategoryController::class, 'index'])->middleware(['auth']);

    Route::get('/category/add', [BrandandcategoryController::class, 'create'])->middleware(['auth'])->name('add.category');
    Route::post('/category/add', [BrandandcategoryController::class, 'store'])->middleware(['auth'])->name('store.category');

    Route::get('/category/edit/{id}', [BrandandcategoryController::class, 'edit'])->middleware(['auth']);
    Route::post('/category/edit/{id}', [BrandandcategoryController::class, 'update'])->middleware(['auth']);

    Route::get('/category/delete/{id}', [BrandandcategoryController::class, 'destroy'])->middleware(['auth']);



    Route::get('/brand', [BrandandcategoryController::class, 'index2'])->middleware(['auth']);

    Route::get('/brand/add', [BrandandcategoryController::class, 'create2'])->middleware(['auth'])->name('add.brand');
    Route::post('/brand/add', [BrandandcategoryController::class, 'store2'])->middleware(['auth'])->name('store.brand');

    Route::get('/brand/edit/{id}', [BrandandcategoryController::class, 'edit2'])->middleware(['auth']);
    Route::post('/brand/edit/{id}', [BrandandcategoryController::class, 'update2'])->middleware(['auth']);

    Route::get('/brand/delete/{id}', [BrandandcategoryController::class, 'destroy2'])->middleware(['auth']);
});


